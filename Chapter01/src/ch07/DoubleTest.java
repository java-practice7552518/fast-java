package ch07;

public class DoubleTest {

	public static void main(String[] args) {
		double dnum = 3.14;
		float fnum = 3.14f;
		
		System.out.println(dnum);
		System.out.println(fnum);
		
		double d = 1.0;
		for (int i=0; i<10000; i++) {
			d += 0.1;
		}
		System.out.println(d);
	}

}
