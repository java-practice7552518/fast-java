package ch20;

public class NestedLoopTest {
    public static void main(String[] args) {

        int dan = 2;
        int cnt = 1;

        while (dan < 10) {
            cnt = 1;
            while (cnt < 10) {
                System.out.printf("%d X %d = %2d\n", dan, cnt, dan * cnt);
                cnt++;
            }
            dan++;
            System.out.println();
        }
    }
}
