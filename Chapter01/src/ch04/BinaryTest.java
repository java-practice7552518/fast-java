package ch04;

public class BinaryTest {

	public static void main(String[] args) {
		int inum = 10;
		int bnum = 0b1010;
		int onum = 012;
		int xnum = 0xA;
		
		System.out.println(inum);
		System.out.println(bnum);
		System.out.println(onum);
		System.out.println(xnum);
	}
}
