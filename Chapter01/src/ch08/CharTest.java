package ch08;

public class CharTest {

	public static void main(String[] args) {
		char ch = 'A';
		System.out.println("==============");
		System.out.println(ch);
		System.out.println((int)ch);

		System.out.printf("%c\n", ch);
		System.out.printf("%d\n", (int)ch);

		char ch2 = 66;
		System.out.println("==============");
		System.out.println(ch2);
		System.out.println((char)ch2);

		int ch3 = 67;
		System.out.println("==============");
		System.out.println(ch3);
		System.out.println((char)ch3);

		char han = '한';
		char uch = '\uD55C';
		System.out.println(han);
		System.out.printf("\\u%X\n", (int)han);

		System.out.println(uch);
	}

}
