package ch11;

public class Operators {

    public static void main(String[] args) {

        inplaceIncrement();
        System.out.println("=============");
        inplaceDecrement();
    }

    public static void inplaceIncrement() {

        int gameScore = 150;

        // int lastScore = ++gameScore;  // gameScore += 1; lastScore = gameScore;
        int lastScore = gameScore++;  // lastScore = gameScore; gameScore += 1;

        System.out.println(lastScore);
        System.out.println(gameScore);
    }

    public static void inplaceDecrement() {

        int gameScore = 150;

        // int lastScore = --gameScore;  // gameScore -= 1; lastScore = gameScore;
        int lastScore = gameScore--;  // lastScore = gameScore; gameScore -= 1;

        System.out.println(lastScore);
        System.out.println(gameScore);
    }
}
