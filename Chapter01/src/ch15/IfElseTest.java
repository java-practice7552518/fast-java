package ch15;

import java.util.Scanner;

public class IfElseTest {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int age = scanner.nextInt();
        scanner.close();

        int charge;

        if (age < 8) {
            charge = 1000;
            System.out.println("미취학 아동입니다.");
        } else if (age < 14) {
            charge = 2000;
            System.out.println("초등학생 입니다.");
        } else if (age < 20) {
            charge = 2500;
            System.out.println("중고등학생 입니다.");
        } else {
            charge = 4000;
            System.out.println("일반인 입니다.");
        }

        System.out.printf("입장료는 %d 원 입니다.\n", charge);
    }
}
