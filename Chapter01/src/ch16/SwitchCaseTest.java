package ch16;

import java.util.Scanner;

public class SwitchCaseTest {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("1~12월을 입력하세요");
        int month = scanner.nextInt();
        scanner.close();

        try {
            int day;
            switch (month) {
                case 1: day = 31; break;
                case 2: day = 28; break;
                case 3: day = 31; break;
                case 4: day = 30; break;
                case 5: day = 31; break;
                case 6: day = 30; break;
                case 7: day = 31; break;
                case 8: day = 31; break;
                case 9: day = 30; break;
                case 10: day = 31; break;
                case 11: day = 30; break;
                case 12: day = 31; break;
                default:
                    throw new Exception("The month must fall in the range of 1 to 12");
            }
            System.out.printf("%d 월은 %d일까지 있습니다.", month, day);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
