package ch11;

public class RelationalTest {

    public static void main(String[] args) {

        int a = 5;
        int b = 3;

        boolean v = (a > b);
        System.out.println(v);

        System.out.println(a < b);
        System.out.println(a <= b);
        System.out.println(a > b);
        System.out.println(a >= b);
        System.out.println(a == b);
        System.out.println(a != b);

    }
}
