package ch11;

public class LogicalTest {

    public static void main(String[] args) {

        int a = 10;
        int b = 20;

        boolean flag = (a > 0) && (b > 0);
        System.out.println(flag);

        flag = (a < 0) && (b > 0);
        System.out.println(flag);

        flag = (a > 0) || (b > 0);
        System.out.println(flag);

        flag = (a < 0) || (b > 0);
        System.out.println(flag);

        flag = !(a > 0);
        System.out.println(flag);
    }
}
