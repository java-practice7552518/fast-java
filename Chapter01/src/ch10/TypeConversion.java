package ch10;

public class TypeConversion {
    public static void main(String[] args) {

        byteToInt();
        intToByte();
        doubleToInt();
        conversionOrder();
    }

    public static void byteToInt() {

        byte bnum = 125;
        int inum = bnum;

        System.out.println(inum);

    }

    public static void intToByte() {

        int inum = 255;
        byte bnum = (byte)inum;

        System.out.println(bnum);
    }

    public static void doubleToInt() {

        double dnum = 3.14;
        int inum = (int) dnum;

        System.out.println(inum);
    }

    public static void conversionOrder() {

        System.out.println("conversionOrder");
        double dnum = 1.2;
        float fnum = 0.9F;

        int inum1 = (int) dnum + (int) fnum;
        int inum2 = (int) (dnum + fnum);

        System.out.println(inum1);
        System.out.println(inum2);
    }
}
