package ch06;

import java.math.BigInteger;

public class VariableTest {

	public static void main(String[] args) {
		byte bnum = 0b1001001;
		System.out.println(bnum);
		
		long lnum = 123465789000L;
		System.out.printf("HEX: %x\n", lnum);
		System.out.println(Integer.toBinaryString(4096));

		BigInteger bignum = new BigInteger("123456789000");
		System.out.println(bignum.toString(2));
	}

}
