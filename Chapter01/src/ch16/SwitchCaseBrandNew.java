package ch16;

import java.util.Scanner;

public class SwitchCaseBrandNew {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("1~12월을 입력하세요");
        int month = scanner.nextInt();
        scanner.close();

        int day = switch (month) {
            case 1, 3 ,5, 7, 8, 10, 12-> 31;
            case 2-> 28;
            case 4, 6, 9, 11-> 30;
            default -> {
                System.out.println("The month must fall in the range of 1 to 12");
                yield -1;
            }
        };

        System.out.printf("%d 월은 %d일까지 있습니다.", month, day);
    }
}
