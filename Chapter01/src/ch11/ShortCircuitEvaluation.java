package ch11;

public class ShortCircuitEvaluation {

    public static void main(String[] args) {

        int a = 10;
        int b = 2;

        boolean v = ((a = a + 10) < 10) && ((b = b + 2) < 10);
        System.out.println(v);
        System.out.println(a);
        System.out.println(b);
        // Naive Expectation: b == 4, but b == 2

        v = ((a = a + 10) < 10) || ((b = b + 2) > 10);
        System.out.println(v);
        System.out.println(a);
        System.out.println(b);
    }
}
